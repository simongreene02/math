# template for typical build.gradle script

## buildscript block

First section is `buildscript` block. This block must come first.
In here define all variables with `ext` prefix.

## plugins block

Second section is `plugins` block. There are two main plugins.
One is `jacoco` plugin. This is test coverage checker.
The other one is `spotless` plugin. This takes care of code format based on
`google-java-format`.

## Automatic task run

Applies code format before compileJava task

    compileJava.dependsOn(spotlessApply)

Creates Html format of jacoco test coverage report in resports/jacoco at the end of the test.
    
    test.finalizedBy(jacocoTestReport)

Fails build if test coverage is below the limit. See jacocoTestCoverageVerification task block.
    
    check.dependsOn(jacocoTestCoverageVerification)



