package com.greatworksinc.math;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DuplicationRemoverTest {

  @Test
  void removeDuplicateCharacters_aaaa_a() {
    assertThat(DuplicationRemover.removeDuplicateCharacters("aaaa")).isEqualTo("a");
  }

  @Test
  void removeDuplicateCharacters_aaeaa_ae() {
    assertThat(DuplicationRemover.removeDuplicateCharacters("aaeaa")).isEqualTo("ae");
  }

  @Test
  void removeDuplicateCharacters_empty() {
    assertThat(DuplicationRemover.removeDuplicateCharacters("")).isEmpty();
  }

  @Test
  void removeDuplicateCharacters_null() {
    assertThrows(
        IllegalArgumentException.class, () -> DuplicationRemover.removeDuplicateCharacters(null));
  }

  @Test
  void removeDuplicateCharacters_aeiou_aeiou() {
    assertThat(DuplicationRemover.removeDuplicateCharacters("aeiou")).isEqualTo("aeiou");
  }

  @Test
  void removeDuplicateCharacters_charArray() {
    char[] array = {'a', 'a', 'a', 'b', 'b', 'a', 'b'};
    char[] expectedOutput = {'a', 'b', ' ', ' ', ' ', ' ', ' '};
    DuplicationRemover.removeDuplicateCharactersArray(array);
    assertThat(array).isEqualTo(expectedOutput);
  }

  @Test
  void removeDuplicateCharactersArrayOptimized() {
    char[] array = {'a', 'a', 'a', 'b', 'b', 'a', 'b'};
    char[] expectedOutput = {'a', 'b', ' ', ' ', ' ', ' ', ' '};
    DuplicationRemover.removeDuplicateCharactersArrayOptimized(array);
    assertThat(array).isEqualTo(expectedOutput);
  }
}
