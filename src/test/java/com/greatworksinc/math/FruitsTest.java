package com.greatworksinc.math;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FruitsTest {

  private Fruits fruits;

  @BeforeEach
  void setUp() {
    fruits = new Fruits();
  }

  @Test
  void giveMeFruits() {
    assertThat(fruits.giveMeFruits())
        .containsExactly("Apple", "Banana", "Cantaloupe", "Durian")
        .inOrder();
  }
}
