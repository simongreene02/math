package com.greatworksinc.math;

import static com.google.common.truth.Truth.assertThat;

import java.util.Arrays;
import org.junit.jupiter.api.Test;

class MatrixZeroTest {

  @Test
  void expandZero() {
    int[][] matrix = {
      {0, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 0, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1}
    };

    int[][] expectedMatrix = {
      {0, 0, 0, 0, 0},
      {0, 1, 0, 1, 1},
      {0, 0, 0, 0, 0},
      {0, 1, 0, 1, 1},
      {0, 1, 0, 1, 1}
    };
    MatrixZero matrixZero = new MatrixZero(matrix);
    matrixZero.expandZero();
    assertThat(Arrays.deepEquals(matrix, expectedMatrix)).isTrue();
  }

  @Test
  void expandZeroNoMatrix() {
    int[][] matrix = {
      {0, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 0, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1}
    };

    int[][] expectedMatrix = {
      {0, 0, 0, 0, 0},
      {0, 1, 0, 1, 1},
      {0, 0, 0, 0, 0},
      {0, 1, 0, 1, 1},
      {0, 1, 0, 1, 1}
    };
    MatrixZero matrixZero = new MatrixZero(matrix);
    matrixZero.expandZeroNoMatrix();
    matrixZero.printMatrix();
    assertThat(Arrays.deepEquals(matrix, expectedMatrix)).isTrue();
  }

  @Test
  void expandZeroNoMatrix_onlyCenter() {
    int[][] matrix = {
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 0, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1}
    };

    int[][] expectedMatrix = {
      {1, 1, 0, 1, 1},
      {1, 1, 0, 1, 1},
      {0, 0, 0, 0, 0},
      {1, 1, 0, 1, 1},
      {1, 1, 0, 1, 1}
    };
    MatrixZero matrixZero = new MatrixZero(matrix);
    matrixZero.expandZeroNoMatrix();
    matrixZero.printMatrix();
    assertThat(Arrays.deepEquals(matrix, expectedMatrix)).isTrue();
  }

  @Test
  void expandZeroNoMatrix_onlyRow() {
    int[][] matrix = {
      {1, 1, 0, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1}
    };

    int[][] expectedMatrix = {
      {0, 0, 0, 0, 0},
      {1, 1, 0, 1, 1},
      {1, 1, 0, 1, 1},
      {1, 1, 0, 1, 1},
      {1, 1, 0, 1, 1}
    };
    MatrixZero matrixZero = new MatrixZero(matrix);
    matrixZero.expandZeroNoMatrix();
    matrixZero.printMatrix();
    assertThat(Arrays.deepEquals(matrix, expectedMatrix)).isTrue();
  }

  @Test
  void expandZeroNoMatrix_onlyCol() {
    int[][] matrix = {
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {0, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1}
    };

    int[][] expectedMatrix = {
      {0, 1, 1, 1, 1},
      {0, 1, 1, 1, 1},
      {0, 0, 0, 0, 0},
      {0, 1, 1, 1, 1},
      {0, 1, 1, 1, 1}
    };
    MatrixZero matrixZero = new MatrixZero(matrix);
    matrixZero.expandZeroNoMatrix();
    matrixZero.printMatrix();
    assertThat(Arrays.deepEquals(matrix, expectedMatrix)).isTrue();
  }

  @Test
  void expandZeroNoMatrix_onlyOnes() {
    int[][] matrix = {
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1}
    };

    int[][] expectedMatrix = {
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 1}
    };
    MatrixZero matrixZero = new MatrixZero(matrix);
    matrixZero.expandZeroNoMatrix();
    matrixZero.printMatrix();
    assertThat(Arrays.deepEquals(matrix, expectedMatrix)).isTrue();
  }

  @Test
  void expandZeroNoMatrix_onlyCenterNonZero() {
    int[][] matrix = {
      {1, 1, 1, 1, 0},
      {1, 1, 1, 1, 0},
      {1, 1, 1, 1, 1},
      {1, 1, 1, 1, 0},
      {0, 0, 1, 0, 0}
    };

    int[][] expectedMatrix = {
      {0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0},
      {0, 0, 1, 0, 0},
      {0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0}
    };
    MatrixZero matrixZero = new MatrixZero(matrix);
    matrixZero.expandZeroNoMatrix();
    matrixZero.printMatrix();
    assertThat(Arrays.deepEquals(matrix, expectedMatrix)).isTrue();
  }
}
