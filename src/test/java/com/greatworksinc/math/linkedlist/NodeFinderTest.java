package com.greatworksinc.math.linkedlist;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class NodeFinderTest {
  private Node<String> a;
  private Node<String> b;
  private Node<String> c;
  private Node<String> d;
  private Node<String> e;

  @BeforeEach
  void setUp() {
    a = new Node<>("apple");
    b = new Node<>("banana");
    c = new Node<>("cantaloupe");
    d = new Node<>("dragonfruit");
    e = new Node<>("eggplant");

    a.setNextNode(b);
    b.setNextNode(c);
    c.setNextNode(d);
    d.setNextNode(e);
  }

  @Test
  void getNode_fiveNodes() {
    assertThat(NodeFinder.findNode(a, 2)).isSameInstanceAs(c);
  }

  @Test
  void getNode_fiveNodes_zeroN() {
    assertThat(NodeFinder.findNode(a, 0)).isSameInstanceAs(e);
  }

  @Test
  @Disabled("TODO: Add exception handling.")
  void getNode_fiveNodes_bigN() {
    assertThat(NodeFinder.findNode(a, 10)).isSameInstanceAs(e);
  }
}
