package com.greatworksinc.math;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StringRotationTest {

  @Test
  void isStringRotation_positive() {
    assertThat(StringRotation.isStringRotation("waterbottle", "erbottlewat")).isTrue();
  }

  @Test
  void isStringRotation_negative_notRotation() {
    assertThat(StringRotation.isStringRotation("watermellon", "erbottlewat")).isFalse();
  }

  @Test
  void isStringRotation_negative_wrongLength() {
    assertThat(StringRotation.isStringRotation("concatenate", "cat")).isFalse();
  }
}
