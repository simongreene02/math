package com.greatworksinc.math;

import com.google.common.collect.ImmutableList;

public class Fruits {

  private static final ImmutableList<String> FRUITS =
      ImmutableList.of("Apple", "Banana", "Cantaloupe", "Durian");

  public ImmutableList<String> giveMeFruits() {
    return FRUITS;
  }
}
