package com.greatworksinc.math.linkedlist;

/** Implement an algorithm to find the nth to last element of a singly linked list. */
public class NodeFinder {
  public static <T> Node<T> findNode(Node<T> head, int n) {
    Node<T> firstPointer = head;
    for (int i = 0; i < n; i++) {
      firstPointer = firstPointer.getNextNode();
    }
    Node<T> secondPointer = head;
    while (firstPointer.getNextNode() != null) {
      firstPointer = firstPointer.getNextNode();
      secondPointer = secondPointer.getNextNode();
    }
    return secondPointer;
  }
}
