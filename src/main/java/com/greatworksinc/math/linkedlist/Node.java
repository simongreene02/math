package com.greatworksinc.math.linkedlist;

public class Node<T> {
  private Node<T> nextNode;
  private final T value;

  public Node(T value) {
    this.value = value;
  }

  public T getValue() {
    return value;
  }

  public Node<T> getNextNode() {
    return nextNode;
  }

  public void setNextNode(Node<T> nextNode) {
    this.nextNode = nextNode;
  }
}
