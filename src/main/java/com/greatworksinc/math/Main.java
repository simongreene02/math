package com.greatworksinc.math;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private static final Logger log = LoggerFactory.getLogger(Main.class);

  private Main() {}

  public static void main(String[] args) {
    new Fruits().giveMeFruits().forEach(log::info);
  }
}
