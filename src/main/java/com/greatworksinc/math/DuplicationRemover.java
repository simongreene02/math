package com.greatworksinc.math;

/**
 * Design an algorithm and write code to remove the duplicate characters in a string without using
 * any additional buffer. NOTE: One or two additional variables are fine. An extra copy of the array
 * is not.
 */
public class DuplicationRemover {
  public static String removeDuplicateCharacters(String inputString) {
    if (inputString == null) {
      throw new IllegalArgumentException();
    }
    String outputString = inputString;
    for (int i = 0; i < outputString.length(); i++) {
      int lastIndex = outputString.lastIndexOf(outputString.charAt(i));
      while (lastIndex != i) {
        outputString = outputString.substring(0, lastIndex) + outputString.substring(lastIndex + 1);
        lastIndex = outputString.lastIndexOf(outputString.charAt(i));
      }
    }
    return outputString;
  }

  private static final char NULL_CHAR = ' ';

  public static void removeDuplicateCharactersArray(char[] letters) {
    if (letters == null) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; i < letters.length - 1; i++) {
      if (letters[i] != NULL_CHAR) {
        for (int j = i + 1; j < letters.length; j++) {
          if (letters[i] == letters[j]) {
            letters[j] = NULL_CHAR;
          }
        }
      }
    }
    for (int i = 1; i < letters.length; i++) {
      if (letters[i] != NULL_CHAR && letters[i - 1] == NULL_CHAR) {
        int index = i;
        while (index > 0 && letters[index - 1] == NULL_CHAR) {
          letters[index - 1] = letters[index];
          letters[index] = NULL_CHAR;
          index--;
        }
      }
    }
  }

  public static void removeDuplicateCharactersArrayOptimized(char[] letters) {
    if (letters == null) {
      throw new IllegalArgumentException();
    }
    for (int i = 0; i < letters.length - 1; i++) {
      if (letters[i] != NULL_CHAR) {
        for (int j = i + 1; j < letters.length; j++) {
          if (letters[i] == letters[j]) {
            letters[j] = NULL_CHAR;
          }
        }
      }
    }
    int nullIndex = 0;
    int charIndex = letters.length - 1;
    while (charIndex > nullIndex && charIndex >= 0 && nullIndex < letters.length) {
      // nullIndex = findFirstNullIndex(letters, nullIndex);
      while (nullIndex < letters.length && letters[nullIndex] != NULL_CHAR) {
        nullIndex++;
      }
      // charIndex = findLastCharacterIndex(letters, charIndex);
      while (charIndex >= 0 && letters[charIndex] == NULL_CHAR) {
        charIndex--;
      }
      if (letters[nullIndex] == NULL_CHAR && letters[charIndex] != NULL_CHAR) {
        swap(letters, nullIndex, charIndex);
        nullIndex++;
        charIndex--;
      }
    }
  }

  private static void swap(char[] array, int index1, int index2) {
    char temp = array[index1];
    array[index1] = array[index2];
    array[index2] = temp;
  }
}
