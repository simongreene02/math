package com.greatworksinc.math;

import com.google.common.collect.ImmutableList;
import java.awt.*;

public class Queens {
  private final int boardHeight;
  private final int boardWidth;
  private final int numQueens;

  public Queens(int boardHeight, int boardWidth, int numQueens) {
    if (numQueens > boardHeight * boardWidth) {
      throw new IllegalArgumentException("There are too many queens to fit on the board.");
    }
    this.boardHeight = boardHeight;
    this.boardWidth = boardWidth;
    this.numQueens = numQueens;
  }

  public void calculateQueenProblem() {
    placeQueen(ImmutableList.of(), null);
  }

  private void placeQueen(ImmutableList<Point> queenList, Point lastQueenLocation) {
    int row = 0;
    int col = -1; // Quick hack to avoid redundant code later on.
    if (lastQueenLocation != null) {
      row = lastQueenLocation.y;
      col = lastQueenLocation.x;
    }
    while (true) {
      col += 1;
      if (col >= boardWidth) {
        col = 0;
        row++;
        if (row >= boardHeight) {
          return;
        }
      }
      Point potentialQueen = new Point(col, row);
      if (isQueenLocationClear(potentialQueen, queenList)) {
        ImmutableList<Point> newQueenList =
            ImmutableList.<Point>builder().addAll(queenList).add(potentialQueen).build();
        if (queenList.size() >= (numQueens - 1)) {
          printBoard(newQueenList);
        } else {
          placeQueen(newQueenList, potentialQueen);
        }
      }
    }
  }

  private boolean isQueenLocationClear(Point queen, ImmutableList<Point> queenList) {
    for (Point otherQueen : queenList) {
      if (queen.x == otherQueen.x) {
        return false;
      }
      if (queen.y == otherQueen.y) {
        return false;
      }
      if (queen.x - queen.y == otherQueen.x - otherQueen.y) {
        return false;
      }
      if (queen.x + queen.y == otherQueen.x + otherQueen.y) {
        return false;
      }
    }
    return true;
  }

  private void printBoard(ImmutableList<Point> queenList) {
    for (int row = 0; row < boardHeight; row++) {
      for (int col = 0; col < boardWidth; col++) {
        if (queenList.contains(new Point(col, row))) {
          System.out.print("Q");
        } else {
          System.out.print(row % 2 == col % 2 ? "+" : "-");
        }
      }
      System.out.println();
    }
    System.out.println();
  }
}
