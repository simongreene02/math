package com.greatworksinc.math;

import java.awt.*;
import java.util.ArrayList;

public class MatrixZero {
  private final int[][] matrix;
  private final int rowSize;
  private final int colSize;

  public MatrixZero(int[][] matrix) {
    // TODO: Validate for equal lengths
    // TODO: Deep copy the array
    this.matrix = matrix;
    this.rowSize = matrix.length;
    this.colSize = matrix[0].length;
  }

  // TODO: Create inner matrix cell/point class

  public void expandZero() {
    ArrayList<Point> pointsToModify = new ArrayList<>();

    for (int row = 0; row < matrix.length; row++) {
      for (int col = 0; col < matrix[row].length; col++) {
        if (matrix[row][col] == 0) {
          pointsToModify.add(new Point(col, row));
        }
      }
    }

    for (Point cell : pointsToModify) {
      for (int row = 0; row < matrix.length; row++) {
        matrix[row][cell.x] = 0;
      }
      for (int col = 0; col < matrix[cell.y].length; col++) {
        matrix[cell.y][col] = 0;
      }
    }
  }

  public void expandZeroNoMatrix() {
    boolean zeroInFirstRow = false;
    boolean zeroInFirstCol = false;

    for (int col = 0; col < colSize; col++) {
      if (matrix[0][col] == 0) {
        zeroInFirstRow = true;
      }
    }
    for (int row = 0; row < rowSize; row++) {
      if (matrix[row][0] == 0) {
        zeroInFirstCol = true;
      }
    }

    for (int row = 1; row < rowSize; row++) {
      for (int col = 1; col < colSize; col++) {
        if (matrix[row][col] == 0) {
          matrix[row][0] = 0;
          matrix[0][col] = 0;
        }
      }
    }

    for (int col = 1; col < colSize; col++) {
      if (matrix[0][col] == 0) {
        for (int row = 0; row < rowSize; row++) {
          matrix[row][col] = 0;
        }
      }
    }
    for (int row = 1; row < rowSize; row++) {
      if (matrix[row][0] == 0) {
        for (int col = 0; col < colSize; col++) {
          matrix[row][col] = 0;
        }
      }
    }

    if (zeroInFirstRow) {
      for (int col = 0; col < colSize; col++) {
        matrix[0][col] = 0;
      }
    }
    if (zeroInFirstCol) {
      for (int row = 0; row < rowSize; row++) {
        matrix[row][0] = 0;
      }
    }
  }

  public void printMatrix() {
    for (int row = 0; row < matrix.length; row++) {
      for (int col = 0; col < matrix[row].length; col++) {
        System.out.print(matrix[row][col] + " ");
      }
      System.out.println();
    }
  }
}
