package com.greatworksinc.math;

public class StringRotation {
  public static boolean isStringRotation(String s1, String s2) {
    return s1.length() == s2.length() && (s1 + s1).contains(s2);
  }
}
